use std::error::Error;
use std::fs;
#[macro_use]
extern crate pest_derive;
use pest::Parser;
use regex::Regex;
#[macro_use]
extern crate lazy_static;

pub struct Config {
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 2 {
            return Err("not enough arguments");
        }

        let filename = args[1].clone();

        Ok(Config { filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let unparsed_file = fs::read_to_string(config.filename).expect("cannot read file {}");
    process(&unparsed_file)
}

#[derive(Parser)]
#[grammar = "plantuml.pest"]
pub struct PlantUmlParser;

fn process(raw_text: &str) -> Result<(), Box<dyn Error>> {
    let mut parsed = PlantUmlParser::parse(Rule::file, &raw_text)?;

    let file = parsed.next().unwrap();

    for item in file.into_inner() {
        match item.as_rule() {
            Rule::class => {
                println!("item {:?}", item);

                for field in item.into_inner() {
                    match field.as_rule() {
                        Rule::field => {
                            let mut field_parts = field.into_inner();
                            let name = field_parts.next().unwrap().as_str();
                            println!("name {:?}", to_screaming_snake_case(name));
                        }
                        _ => (),
                    }
                }
            }
            _ => (),
        }
    }

    Ok(())
}

fn to_screaming_snake_case(name: &str) -> String {
    lazy_static! {
        static ref SSC: Regex = Regex::new(r"^[A-Z_]+$").unwrap();
    }
    if SSC.is_match(name) {
        return String::from(name);
    }

    let mut indices: Vec<usize> = name
        .char_indices()
        .filter(|(_, chr)| chr.is_uppercase())
        .map(|(idx, _)| idx)
        .collect();

    if let Some(&val) = indices.first() {
        if val == 0 {
            indices.remove(0);
        }
    }

    let mut end_indices = indices.clone();

    indices.insert(0, 0);
    end_indices.push(name.len());

    let parts: Vec<String> = indices
        .iter()
        .zip(end_indices.iter())
        .map(|(&start, &end)| &name[start..end])
        .map(|part| part.to_uppercase())
        .collect();
    parts.join("_")
}

#[cfg(test)]
mod tests {
    use super::*;
    use test_case::test_case;

    #[test_case( "lastName", "LAST_NAME" ; "two parts")]
    #[test_case( "OrdinaryPerson", "ORDINARY_PERSON" ; "uppercase initial")]
    #[test_case( "eMail", "E_MAIL" ; "single character")]
    #[test_case( "ALREADY_DESIRED_CASE", "ALREADY_DESIRED_CASE" ; "already screaming snake case")]
    fn verify_to_screaming_snake_case(text: &str, expected: &str) {
        let actual = to_screaming_snake_case(text);

        assert_eq!(expected, actual);
    }
}
